	var okCoinWebSocket = {};
	okCoinWebSocket.init = function(uri, apiKey, secretKey) {
		this.wsUri = uri;
		this.apiKey = apiKey;
		this.secretKey = secretKey;
		this.lastHeartBeat = new Date().getTime();
		this.overtime = 8000;
		
		okCoinWebSocket.websocket = new WebSocket(okCoinWebSocket.wsUri);
		
		okCoinWebSocket.websocket.onopen = function(evt) {
			onOpen(evt)
		};
		okCoinWebSocket.websocket.onclose = function(evt) {
			onClose(evt)
		};
		okCoinWebSocket.websocket.onmessage = function(evt) {
			onMessage(evt)
		};
		okCoinWebSocket.websocket.onerror = function(evt) {
			onError(evt)
		};
		
		//setInterval(checkConnect,5000);
	}